# 2021_08_ESTRO


## mini orals

Use the built-in continuous integration in GitLab.

- [x] Jintao Ren et al. [End-to-end head & neck tumor auto-segmentation using CT/PET and MRI without deformable registration](https://www.estro.org/Abstract?a=bf7ac0ff-b97d-eb11-a812-0022489bcff8)

## proffered papers